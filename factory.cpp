//============================================================================
// Name        : factory.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "CharacterFactory.h"
using namespace std;

int main() {
	CharacterFactory& factory = CharacterFactory::getInstance();
	Character* character[2];
	character[0]= &factory.createCharacter(CharacterFactory::player, "Rafael");
	character[1]= &factory.createCharacter(CharacterFactory::enemy, "Roger");

	character[0]->speak();
	character[1]->speak();

	return 0;
}
