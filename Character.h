/*
 * Character.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef CHARACTER_H_
#define CHARACTER_H_

#include <string>

class Character {
public:
	Character();
	Character(std::string name);
	virtual ~Character();
	const std::string& getIntentions() const {return intentions;}
	virtual void speak() = 0;

protected:
	std::string name;
	std::string intentions;
};

#endif /* CHARACTER_H_ */
