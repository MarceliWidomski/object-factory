/*
 * CharacterFactory.cpp
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#include "CharacterFactory.h"
#include <iostream>

CharacterFactory::~CharacterFactory() {
	// TODO Auto-generated destructor stub
}
CharacterFactory& CharacterFactory::getInstance() {
	static CharacterFactory *ptrCharacterFactory = new CharacterFactory;
	return *ptrCharacterFactory;
}
Character& CharacterFactory::createCharacter(characterType type, std::string name) {
	Character *ptrCharacter;
	switch (type) {
	case player:
		ptrCharacter = new Player(name);
		break;
	case enemy:
		ptrCharacter = new Enemy(name);
		break;
	default:
		std::cout << "Creating character failed" << std::endl;
		ptrCharacter = 0;
		break;
	}
	return *ptrCharacter;
}

