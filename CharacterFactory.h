/*
 * CharacterFactory.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef CHARACTERFACTORY_H_
#define CHARACTERFACTORY_H_

#include "Character.h"
#include "Player.h"
#include "Enemy.h"
#include <string>

class CharacterFactory {
public:
	enum characterType {player = 1, enemy};
	virtual ~CharacterFactory();
	static CharacterFactory& getInstance();
	Character& createCharacter(characterType type, std::string name = "");
private:
	CharacterFactory() {}
	CharacterFactory(const CharacterFactory& other) {}
};

#endif /* CHARACTERFACTORY_H_ */
