/*
 * Player.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "Character.h"

class Player: public Character {
public:
	Player();
	Player(std::string name);
	virtual ~Player();
	void speak();

};

#endif /* PLAYER_H_ */
