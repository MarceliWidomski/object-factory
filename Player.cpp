/*
 * Player.cpp
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#include "Player.h"
#include <iostream>

Player::Player() {
	intentions = "good";
}
Player::Player(std::string name) :
		Character(name) {
	intentions = "good";
}

Player::~Player() {
	// TODO Auto-generated destructor stub
}
void Player::speak() {
	std::cout << "My name is " << name << ". "
			<< "I'm player, my purpose is to complete campaign. My intentions are "
			<< intentions << "." << std::endl;
}
