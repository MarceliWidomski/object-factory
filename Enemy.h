/*
 * Enemy.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef ENEMY_H_
#define ENEMY_H_

#include "Character.h"

class Enemy: public Character {
public:
	Enemy();
	Enemy(std::string name);
	virtual ~Enemy();
	void speak();

};

#endif /* ENEMY_H_ */
