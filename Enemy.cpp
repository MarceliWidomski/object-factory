/*
 * Enemy.cpp
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#include "Enemy.h"
#include <iostream>

Enemy::Enemy() {
	intentions = "hostile";
}
Enemy::Enemy(std::string name) :
		Character(name) {
	intentions = "hostile";
}
Enemy::~Enemy() {
	// TODO Auto-generated destructor stub
}
void Enemy::speak(){
	std::cout << "My name is " << name << ". "
			<< "I'm your enemy, my purpose is to fail your mission. My intentions are "
			<< intentions << "." << std::endl;
}

